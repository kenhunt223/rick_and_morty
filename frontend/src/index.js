import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider as StateProvider } from 'react-redux';

import reportWebVitals from './reportWebVitals';
import ErrorLayout from './Layouts/ErrorLayout/ErrorLayout';
import AppProvider from './AppContext/AppContext';

import App from './App/App';
import store from 'Store';

import './Styles.scss';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ErrorLayout>
        <AppProvider>
          <StateProvider store={store}>
            <App />
          </StateProvider>
        </AppProvider>
      </ErrorLayout>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
