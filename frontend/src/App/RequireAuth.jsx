import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSession } from 'Hooks';
import Router from 'Router';

const RequireAuth = ({ children }) => {
  const {
    session: { isAuthenticated },
  } = useSession();

  return isAuthenticated === true ? children : <Navigate to={Router.auth} replace />;
};

export default RequireAuth;
