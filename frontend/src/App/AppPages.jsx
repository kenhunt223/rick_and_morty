import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Router from 'Router';

import Characters from 'Pages/Characters';
import CharacterDetails from 'Pages/CharacterDetails';
import Authenticate from 'Pages/Authenticate';
import PageNotFound from 'Pages/404';

import RequireAuth from './RequireAuth';

const AppPages = () => (
  <Routes>
    <Route
      path={Router.characters}
      element={
        <RequireAuth>
          <Characters />
        </RequireAuth>
      }
    />

    <Route
      path={Router.character}
      element={
        <RequireAuth>
          <CharacterDetails />
        </RequireAuth>
      }
    />

    <Route path={Router.auth} element={<Authenticate />} />
    <Route path='*' element={<PageNotFound />} />
  </Routes>
);

export default AppPages;
