import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { useSession } from 'Hooks';

import Navbar from 'Components/Navbar';
import Toast from 'Components/Toast';
import ProcessingSpinner from 'Components/ProcessingSpinner';

import AppPages from './AppPages';

function App() {
  // * Determines whether the [token] from [LocalStorage] is validated or not
  // * Just blocks the UI while it's being validated...
  // * Does Not affect to the behavior of the application or authentication process
  const [isSessionTokenValidated, setIsSessionTokenValidated] = useState(false);

  const { ui } = useSelector((state) => state);
  const { toast } = ui || {};

  const {
    endSession,
    validateSession,
    session: { isAuthenticated, user },
  } = useSession();

  // * While logging out, whether the call was successful or not - for the User it's not important
  // * 1. End the session, so the User sees what expected (Login page)
  // * 2. Register telemetries if something went wrong during the logout
  const onLogout = () => {
    endSession();
  };

  const _validateSession = async () => {
    await validateSession();

    setIsSessionTokenValidated(true);
  };

  useEffect(() => {
    _validateSession();
  }, []);

  return (
    <div className='App'>
      {isSessionTokenValidated ? (
        <>
          {isAuthenticated && <Navbar onLogout={onLogout} username={user.username} />}

          <AppPages />
        </>
      ) : (
        <ProcessingSpinner />
      )}

      {toast.message && (
        <Toast
          isFixed={toast.isFixed}
          message={toast.message}
          isErrorType={toast.isErrorType}
          timeout={toast.timeout}
          className={toString.className}
        />
      )}
    </div>
  );
}

export default App;
