import uiInitialState from './uiInitialState';

const initialState = {
  ui: uiInitialState,
};

export default initialState;
