const uiInitialState = {
  characters: [],
  toast: {
    className: '',
    message: '',
    timeout: undefined,
    isFixed: false,
  },
};

export default uiInitialState;
