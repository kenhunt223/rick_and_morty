import { createStore, combineReducers } from 'redux';
import reducers from './Reducers';
import Actions from './Actions';

const reducer = combineReducers({
  ...reducers,
});

const store = createStore(reducer);

export default store;

export { Actions };
