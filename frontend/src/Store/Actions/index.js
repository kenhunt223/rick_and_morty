import uiActions from './uiActions';

const Actions = {
  ui: uiActions,
};

export default Actions;
