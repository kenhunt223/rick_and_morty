import actions from '../Actions';
import initialState from '../InitialState';

const defaultAction = {
  payload: undefined,
  type: undefined,
};

const { ui: uiActions } = actions;
const { ui: uiState } = initialState;

const uiReducer = (state = uiState, action = defaultAction) => {
  switch (action.type) {
    case uiActions.SET_TOAST_PROPS:
      return {
        ...state,
        toast: {
          ...state.toast,
          ...action.payload,
        },
      };

    case uiActions.SET_CHARACTERS:
      return {
        ...state,
        characters: action.payload,
      };

    case uiActions.RESET_UI:
      return uiState;

    case uiActions.UPDATE_CHARACTERS_FAVORITE:
      const { characters } = state;

      if (!characters.length) return state;

      const characterIndex = characters.findIndex(({ id }) => id === action.payload.id);
      if (characterIndex > -1) {
        characters[characterIndex].isFavorite = action.payload.isFavorite;
      }

      return {
        ...state,
        characters,
      };

    default:
      return state;
  }
};

export default uiReducer;
