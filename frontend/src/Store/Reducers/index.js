import uiReducer from './uiReducer';

const reducers = {
  ui: uiReducer,
};

export default reducers;
