import React from 'react';
import Text from 'Components/Text';
class ErrorLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return (
        <div className='ErrorLayout'>
          <Text fontSize='lg'>Something went wrong...</Text>
        </div>
      );
    }

    return children;
  }
}

export default ErrorLayout;
