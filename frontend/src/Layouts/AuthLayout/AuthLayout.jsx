import React from 'react';

const AuthLayout = ({ children }) => {
  return <div className='AuthLayout'>{children}</div>;
};

export default AuthLayout;
