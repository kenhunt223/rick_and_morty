import {
  saveToLocalStorage,
  getFromLocalStorage,
  removeFromLocalStorage,
} from './LocalStorageService';
import HTTPService from './HTTPService';

const LocalStorageService = {
  saveToLocalStorage,
  getFromLocalStorage,
  removeFromLocalStorage,
};

export { LocalStorageService, HTTPService };
