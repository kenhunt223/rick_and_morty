export function saveToLocalStorage(key, value) {
  try {
    localStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    return {
      error: e,
    };
  }
}

export function getFromLocalStorage(key) {
  try {
    const item = localStorage.getItem(key);

    return { item: JSON.parse(item) };
  } catch (e) {
    return {
      error: e,
    };
  }
}

export function removeFromLocalStorage(key) {
  try {
    localStorage.removeItem(key);
  } catch (e) {
    return {
      error: e,
    };
  }
}
