import { getFromLocalStorage } from './LocalStorageService';

class HTTPService {
  static instance;
  token = null;
  baseUrl = '';

  constructor() {
    this.init = this.init.bind(this);
    this.validateSession = this.validateSession.bind(this);
    this.login = this.login.bind(this);
    this.signUp = this.signUp.bind(this);
    this.getCharacters = this.getCharacters.bind(this);
    this.getCharacter = this.getCharacter.bind(this);
    this.toggleFavorite = this.toggleFavorite.bind(this);
    this.destroy = this.destroy.bind(this);
  }

  getInstance() {
    if (!HTTPService.instance) {
      HTTPService.instance = new HTTPService();
    }

    return HTTPService.instance;
  }

  init({ tokenKey, baseUrl }) {
    const { item } = getFromLocalStorage(tokenKey);

    this.token = item;
    this.baseUrl = baseUrl || this.baseUrl;
  }

  async validateSession() {
    try {
      const res = await fetch(`${this.baseUrl}/validate-session`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.token}`,
        },
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  async login(user) {
    try {
      const res = await fetch(`${this.baseUrl}/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  async signUp(newUser) {
    try {
      const res = await fetch(`${this.baseUrl}/signup`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newUser),
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  async getCharacters() {
    try {
      const res = await fetch(`${this.baseUrl}/characters`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.token}`,
        },
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  async getCharacter(id) {
    try {
      const res = await fetch(`${this.baseUrl}/character/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.token}`,
        },
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  async toggleFavorite(id) {
    try {
      const res = await fetch(`${this.baseUrl}/toggle-favorite`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.token}`,
        },
        body: JSON.stringify({ id }),
      });

      const { data, message } = await res.json();

      return { status: res.status, message, data };
    } catch (e) {
      return {
        status: 400,
        message: e.message,
      };
    }
  }

  destroy() {
    this.token = null;
  }
}

export default HTTPService;
