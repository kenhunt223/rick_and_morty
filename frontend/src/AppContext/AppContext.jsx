import React, { createContext, useReducer } from 'react';
import reducer from './rootReducer';
import initialState from './InitialState';

export const AppContext = createContext(initialState);

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ ...state, dispatchContext: dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
