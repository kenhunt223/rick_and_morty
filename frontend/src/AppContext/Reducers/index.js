import authReducer from './authReducer';

const contextReducers = {
  auth: authReducer,
};

export default contextReducers;
