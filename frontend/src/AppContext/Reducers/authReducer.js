import actions from '../Actions';
import initialState from '../InitialState';

const defaultAction = {
  payload: undefined,
  type: undefined,
};

const { auth: authActions } = actions;

const authReducer = (state = initialState, action = defaultAction) => {
  switch (action.type) {
    case authActions.LOGIN:
      return {
        ...state,
        auth: {
          isAuthenticated: true,
          user: action.payload.user,
          token: action.payload.token,
        },
      };

    case authActions.LOGOUT:
      return {
        ...state,
        auth: {
          isAuthenticated: false,
          user: undefined,
          token: undefined,
        },
      };

    default:
      return state;
  }
};

export default authReducer;
