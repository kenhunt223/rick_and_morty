import actions from './Actions';
import initialState from './InitialState';
import reducers from './Reducers';

const rootReducer = (state = initialState, action = {}) => {
  const authActions = Object.values(actions.auth);

  // * Same approach for the other Actions
  // * example: If we had actions for UI (theme, settings etc...), we could do something like this:
  // const uiActions = Object.values(actions.ui);

  if (authActions.includes(action.type)) {
    return reducers.auth(state, action);
  }

  // * Same approach for the other Reducers
  // * example: If we had a reducer for the UI (theme, settings etc...), we would have to add it here
  // if (uiActions.includes(action.type)) {
  //   return reducers.ui(state, action);
  // }

  return state;
};

export default rootReducer;
