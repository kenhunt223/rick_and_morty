const contextAuthInitialState = {
  token: undefined,
  user: undefined,
  isAuthenticated: false,
};

export default contextAuthInitialState;
