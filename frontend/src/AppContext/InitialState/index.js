import contextAuthInitialState from './contextAuthInitialState';

const contextInitialState = {
  auth: contextAuthInitialState,
};

export default contextInitialState;
