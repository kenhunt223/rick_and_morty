import contextAuthActions from './contextAuthActions';

const ContextActions = {
  auth: contextAuthActions,
};

export default ContextActions;
