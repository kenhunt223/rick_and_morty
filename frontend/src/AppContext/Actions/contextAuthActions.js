const contextAuthActions = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

export default contextAuthActions;
