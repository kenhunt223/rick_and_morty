const Router = {
  auth: '/auth',
  characters: '/',
  character: '/characters/:id',
};

export default Router;
