import React from 'react';

const Text = ({ fontSize, children, isDark, isLabel, className = '' }) => {
  const getType = () => {
    switch (fontSize) {
      case 'sm':
        return 'sm';
      case 'md':
        return 'md';
      case 'lg':
        return 'lg';
      default:
        return 'md';
    }
  };

  return (
    <p
      className={`Text ${className} ${getType()}${isDark ? ' dark' : ''}${isLabel ? ' label' : ''}`}
    >
      {children}
    </p>
  );
};

export default Text;
