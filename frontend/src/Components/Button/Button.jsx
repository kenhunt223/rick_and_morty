import React from 'react';
import { Spinner } from '../Icons';

const Button = ({ children, className = '', onClick, isLoading, isDisabled }) => {
  const _onClick = (e) => {
    if (isLoading || isDisabled) return;

    onClick(e);
  };

  return (
    <button
      className={`Button ${className} ${isDisabled ? 'disabled' : ''}`}
      onClick={_onClick}
      disabled={isDisabled}
    >
      {children}

      {isLoading && <Spinner />}
    </button>
  );
};

export default Button;
