import React from 'react';

import Button from 'Components/Button';
import Text from 'Components/Text';

const Navbar = ({ onLogout, username }) => (
  <div className='Navbar'>
    <Text className='Navbar--title' fontSize='lg'>
      {`hello ${username}`}
    </Text>

    <Button onClick={onLogout}>
      <Text isDark>Logout</Text>
    </Button>
  </div>
);

export default Navbar;
