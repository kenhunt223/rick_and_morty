import React, { useEffect, useRef } from 'react';

const Input = ({
  label,
  value = '',
  type,
  name,
  placeholder,
  onChange,
  isAutoFocus,
  className = '',
}) => {
  const inputRef = useRef();

  useEffect(() => {
    if (isAutoFocus) {
      inputRef.current.focus();
    }
  }, []);

  return (
    <div className={`Input ${className}`}>
      {label && <label>{label}</label>}

      <input
        ref={inputRef}
        type={type}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      ></input>
    </div>
  );
};

export default Input;
