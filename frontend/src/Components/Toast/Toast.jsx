import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Actions } from 'Store';

const Toast = ({ message, isErrorType, className = '' }) => {
  const [isShowing, setIsShowing] = useState('showToast');

  const dispatch = useDispatch();
  const { ui: uiActions } = Actions;

  const hideToast = () => {
    setIsShowing('hideToast');

    setTimeout(() => {
      dispatch({ type: uiActions.SET_TOAST_PROPS, payload: { message: null } });
    }, 500);
  };

  useEffect(() => {
    setTimeout(() => {
      hideToast();
    }, 1500);
  }, []);

  return (
    <div className={`Toast--container${isErrorType ? ' error ' : ''} ${isShowing} ${className}`}>
      {message}
    </div>
  );
};

export default Toast;
