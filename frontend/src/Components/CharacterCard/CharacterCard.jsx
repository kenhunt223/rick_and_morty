import { Favorite } from 'Components/Icons';
import Text from 'Components/Text';
import React from 'react';

const CharacterCard = ({ character, onToggleFavorite }) => {
  const { image, name, species, status, origin, isFavorite } = character;

  const _onClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    onToggleFavorite();
  };

  const statusText = status === 'Alive' ? 'alive' : 'dead';
  const completeStatus = `${statusText} - ${species}`;

  return (
    <div className='CharacterCard'>
      <img src={image} alt={name} />

      <div className='CharacterCard--details'>
        <div className='CharacterCard--details--title'>
          <Text fontSize='lg'>{name}</Text>
          <Text className='status' fontSize='sm'>
            {completeStatus}
          </Text>
        </div>

        <div className='CharacterCard--details--description'>
          <Text isLabel fontSize='sm'>
            origin
          </Text>
          <Text className='origin' fontSize='sm'>
            {origin.name}
          </Text>

          <Favorite isActive={isFavorite} onClick={_onClick} />
        </div>
      </div>
    </div>
  );
};

export default CharacterCard;
