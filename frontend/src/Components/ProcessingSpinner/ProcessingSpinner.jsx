import React from 'react';

import Text from '../Text';
import { Spinner } from '../Icons';

const ProcessingSpinner = ({ className = '', label = 'Processing' }) => (
  <div className={`ProcessingSpinner ${className}`}>
    <Text>{label}</Text>

    <Spinner />
  </div>
);

export default ProcessingSpinner;
