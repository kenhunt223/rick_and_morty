import { useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import { ContextActions, AppContext } from 'AppContext';
import { LocalStorageService, HTTPService } from 'Services';
import { useUI } from '.';

import Router from 'Router';

function useSession() {
  const {
    dispatchContext,
    auth: { token, user, isAuthenticated },
  } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();
  const httpService = new HTTPService().getInstance();
  const { resetUI } = useUI();

  const initHTTPService = () => {
    httpService.init({
      tokenKey: process.env.REACT_APP_TOKEN_KEY,
      baseUrl: process.env.REACT_APP_BASE_URL,
    });
  };

  const startSession = ({ user, token }) => {
    dispatchContext({ type: ContextActions.auth.LOGIN, payload: { user, token } });
    LocalStorageService.saveToLocalStorage(process.env.REACT_APP_TOKEN_KEY, token);

    initHTTPService();

    const nextPath = location.pathname === Router.auth ? Router.characters : location.pathname;
    navigate(nextPath, { replace: true });
  };

  const endSession = () => {
    dispatchContext({ type: ContextActions.auth.LOGOUT });
    LocalStorageService.removeFromLocalStorage(process.env.REACT_APP_TOKEN_KEY);

    resetUI();
    httpService.destroy();

    navigate(Router.auth, { replace: true });
  };

  const validateSession = async () => {
    initHTTPService();

    const { status, data } = await httpService.validateSession();

    if (status === 200 || status === 201) {
      startSession(data);
    } else {
      endSession();
    }

    return true;
  };

  return {
    startSession,
    endSession,
    validateSession,
    initHTTPService,
    session: {
      token,
      user,
      isAuthenticated,
    },
  };
}

export default useSession;
