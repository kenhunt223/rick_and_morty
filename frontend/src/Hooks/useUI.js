import { useDispatch } from 'react-redux';

import { Actions } from 'Store';

function useUI() {
  const dispatch = useDispatch();

  const showToast = ({ message, isErrorType }) => {
    dispatch({
      type: Actions.ui.SET_TOAST_PROPS,
      payload: {
        message,
        isErrorType,
      },
    });
  };

  const setCharacters = (data) => {
    dispatch({
      type: Actions.ui.SET_CHARACTERS,
      payload: data,
    });
  };

  const resetUI = () => {
    dispatch({
      type: Actions.ui.RESET_UI,
    });
  };

  const updateCharactersFavorite = (data) => {
    dispatch({
      type: Actions.ui.UPDATE_CHARACTERS_FAVORITE,
      payload: data,
    });
  };

  return {
    showToast,
    setCharacters,
    resetUI,
    updateCharactersFavorite,
  };
}

export default useUI;
