import React, { useState, useRef, useEffect } from 'react';
import { HTTPService } from 'Services';

import { useSession, useUI } from 'Hooks';

import AuthLayout from 'Layouts/AuthLayout';
import Form from './Form/Form';

const SUBMIT_TYPES = {
  LOGIN: 'LOGIN',
  SIGN_UP: 'SIGN_UP',
};

const Authenticate = () => {
  const [isLoginScreen, setIsLoginScreen] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [fields, setFields] = useState({
    username: '',
    password: '',
  });

  const httpServiceRef = useRef();
  const { startSession } = useSession();
  const { showToast } = useUI();

  useEffect(() => {
    httpServiceRef.current = new HTTPService().getInstance();
  }, []);

  const onSubmitTypeChange = () => {
    setFields({
      username: '',
      password: '',
    });

    setIsLoginScreen(!isLoginScreen);
  };

  const onFieldChange = (e) => {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  };

  const onSubmit = async ({ e, type }) => {
    e.preventDefault();

    const { username, password } = fields;

    let status = undefined;
    let data = undefined;
    let message = undefined;

    setIsLoading(true);
    if (type === SUBMIT_TYPES.LOGIN) {
      ({ data, status, message } = await httpServiceRef.current.login({
        username,
        password,
      }));
    } else {
      ({ data, status, message } = await httpServiceRef.current.signUp({
        username,
        password,
      }));
    }
    setIsLoading(false);

    if (status === 200 || status === 201) {
      startSession(data);
    } else {
      showToast({ message, isErrorType: true });
    }
  };

  const changeAuthTypeText = isLoginScreen ? 'Sign Up' : 'Login';

  return (
    <AuthLayout>
      <div className='Authenticate'>
        {isLoginScreen ? (
          <Form
            fields={fields}
            onSubmit={(e) => onSubmit({ e, type: SUBMIT_TYPES.LOGIN })}
            submitText='Login'
            title='Login'
            onFieldChange={onFieldChange}
            isLoading={isLoading}
          />
        ) : (
          <Form
            fields={fields}
            onSubmit={(e) => onSubmit({ e, type: SUBMIT_TYPES.SIGN_UP })}
            submitText='Sign Up'
            title='Sign Up'
            onFieldChange={onFieldChange}
            isLoading={isLoading}
          />
        )}

        <p className='Authenticate--switch' onClick={onSubmitTypeChange}>
          {changeAuthTypeText}
        </p>
      </div>
    </AuthLayout>
  );
};

export default Authenticate;
