import React from 'react';
import Input from 'Components/Input';
import Button from 'Components/Button';
import Text from 'Components/Text';

const Form = ({ onSubmit, title, fields, submitText, onFieldChange, isLoading }) => {
  const { username, password } = fields;

  const isSubmitDisabled = !username || !password;

  return (
    <form className='Form'>
      <Text className='title' fontSize='lg'>
        {title}
      </Text>
      <Input
        isAutoFocus
        type='text'
        placeholder='Username'
        name='username'
        value={username}
        onChange={onFieldChange}
      />
      <Input
        type='password'
        placeholder='Password'
        name='password'
        value={password}
        onChange={onFieldChange}
      />
      <Button type='submit' onClick={onSubmit} isLoading={isLoading} isDisabled={isSubmitDisabled}>
        <Text isDark>{submitText}</Text>
      </Button>
    </form>
  );
};

export default Form;
