import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useUI } from 'Hooks';
import { HTTPService } from 'Services';

import MainLayout from 'Layouts/MainLayout';
import ProcessingSpinner from 'Components/ProcessingSpinner';
import Text from 'Components/Text';
import { Favorite } from 'Components/Icons';

const CharacterDetails = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [character, setCharacter] = useState(undefined);
  const httpService = new HTTPService().getInstance();
  const { showToast, updateCharactersFavorite } = useUI();
  const { id } = useParams();

  const loadCharacter = async () => {
    setIsLoading(true);
    const { status, data, message } = await httpService.getCharacter(id);
    setIsLoading(false);

    if (status === 200) {
      setCharacter(data);
    } else {
      showToast({ message, isErrorType: true });
    }
  };

  const onToggleFavorite = async () => {
    setIsLoading(true);
    const { status, message, data } = await httpService.toggleFavorite(character.id);
    setIsLoading(false);

    if (status === 200 || status === 201) {
      updateCharactersFavorite(data);

      setCharacter({ ...character, isFavorite: data.isFavorite });
    } else {
      showToast({ message, isErrorType: true });
    }
  };

  useEffect(() => {
    loadCharacter();
  }, []);

  const { episode, gender, image, location, name, origin, species, status, isFavorite } =
    character || {};

  const characterTemplate = character ? (
    <div className='CharacterDetails'>
      <div className='CharacterDetails--description'>
        <img src={image} alt={name} />

        <div className='CharacterDetails--description--credentials'>
          <Text className='CharacterDetails--description--credentials--name' fontSize='lg'>
            {name}
          </Text>

          <div className='CharacterDetails--description--credentials--species'>
            <Text isLabel fontSize='sm'>
              specie:
            </Text>
            <Text>{species}</Text>
          </div>

          <div className='CharacterDetails--description--credentials--gender'>
            <Text isLabel fontSize='sm'>
              gender:
            </Text>
            <Text>{gender}</Text>
          </div>

          <div className='CharacterDetails--description--credentials--origin'>
            <Text isLabel fontSize='sm'>
              origin:
            </Text>
            <Text>{origin.name}</Text>
          </div>

          <Favorite isActive={isFavorite} onClick={onToggleFavorite} />
        </div>
      </div>

      <div className='CharacterDetails--additionalDetails'>
        <div className='CharacterDetails--additionalDetails--status'>
          <Text isLabel fontSize='sm'>
            status:
          </Text>
          <Text>{status}</Text>
        </div>

        <div className='CharacterDetails--additionalDetails--lastLocation'>
          <Text isLabel fontSize='sm'>
            last seen in:
          </Text>
          <Text>{location.name}</Text>
        </div>

        <div className='CharacterDetails--additionalDetails--explorations'>
          <Text className='CharacterDetails--additionalDetails--explorations--title' isLabel>
            Explored worlds:
          </Text>
          <Text>{episode.length}</Text>
        </div>
      </div>
    </div>
  ) : null;

  return (
    <MainLayout>
      {isLoading && <ProcessingSpinner />}

      {characterTemplate}
    </MainLayout>
  );
};

export default CharacterDetails;
