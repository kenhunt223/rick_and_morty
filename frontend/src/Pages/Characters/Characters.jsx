import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { HTTPService } from 'Services';
import { useUI } from 'Hooks';

import MainLayout from 'Layouts/MainLayout/MainLayout';
import ProcessingSpinner from 'Components/ProcessingSpinner';
import CharacterCard from 'Components/CharacterCard/CharacterCard';

const Characters = () => {
  const [isLoading, setIsLoading] = useState(false);
  const httpService = new HTTPService().getInstance();
  const { showToast, setCharacters, updateCharactersFavorite } = useUI();
  const { ui } = useSelector((state) => state);
  const { characters } = ui || {};

  const loadCharacters = async () => {
    setIsLoading(true);
    const { status, data, message } = await httpService.getCharacters();
    setIsLoading(false);

    if (status === 200) {
      setCharacters(data);
    } else {
      showToast({ message, isErrorType: true });
    }
  };

  const onToggleFavorite = async (characterId) => {
    setIsLoading(true);
    const { status, message, data } = await httpService.toggleFavorite(characterId);
    setIsLoading(false);

    if (status === 200 || status === 201) {
      updateCharactersFavorite(data);
    } else {
      showToast({ message, isErrorType: true });
    }
  };

  useEffect(() => {
    if (!characters.length) loadCharacters();
  }, []);

  return (
    <MainLayout>
      {isLoading && <ProcessingSpinner />}

      <div className='Characters'>
        {!!characters.length &&
          characters.map((character) => (
            <Link
              key={character.id}
              to={`/characters/${character.id}`}
              className='CharacterCardWrapper'
            >
              <CharacterCard
                character={character}
                onToggleFavorite={() => onToggleFavorite(character.id)}
              />
            </Link>
          ))}
      </div>
    </MainLayout>
  );
};

export default Characters;
