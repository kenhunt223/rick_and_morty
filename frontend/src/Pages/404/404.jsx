import React from 'react';
import MainLayout from 'Layouts/MainLayout/MainLayout';
import Text from 'Components/Text';

const PageNotFound = () => (
  <MainLayout>
    <div className='PageNotFound'>
      <Text fontSize='lg'>NOT FOUND</Text>
      <Text>You just hit a route that doesn't exist... the sadness.</Text>
    </div>
  </MainLayout>
);

export default PageNotFound;
