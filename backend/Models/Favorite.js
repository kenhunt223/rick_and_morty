import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const FavoriteSchema = new Schema({
  userId: String,
  characterIds: [String],
});

const Favorite = mongoose.model('Favorites', FavoriteSchema);

export default Favorite;
