import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: String,
  password: String,
  id: String,
});

const User = mongoose.model('Users', UserSchema);

export default User;
