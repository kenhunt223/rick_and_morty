import dotenv from 'dotenv';
dotenv.config();
import mongoose from 'mongoose';
import express from 'express';
import cors from 'cors';

import AuthMiddleware from './Middlewares/AuthMiddleware.js';

import {
  Characters,
  CharacterDetails,
  Login,
  Signup,
  ToggleFavorite,
  ValidateSession,
} from './Router/index.js';

const DB_URL = process.env.DB_URL;

const app = express();

app.use(express.json());
app.use(cors());

app.use('/', AuthMiddleware);

app.use('/validate-session', ValidateSession);
app.use('/signup', Signup);
app.use('/login', Login);
app.use('/characters', Characters);
app.use('/character', CharacterDetails);
app.use('/toggle-favorite', ToggleFavorite);

mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('DB connected');

    app.listen(4000);
  })
  .catch((err) => {
    console.log(err);
  });
