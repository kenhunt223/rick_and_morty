import jwt from 'jsonwebtoken';

const AuthMiddleware = async (req, res, next) => {
  try {
    const { headers, url } = req || {};
    const { authorization } = headers || {};

    const isNewSession = url === '/login' || url === '/signup';

    if (isNewSession) return next();

    const token = authorization && authorization.split(' ')[1];
    if (!token) return res.status(401).json({ message: 'No token provided' });

    jwt.verify(token, process.env.APP_SECRET_KEY, (err, user) => {
      if (err) return res.status(403).json({ message: 'Invalid token' });

      req.user = user;

      return next();
    });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
};

export default AuthMiddleware;
