import express from 'express';
import Favorite from '../../Models/Favorite.js';
import fetch from 'node-fetch';

const router = express.Router();

router.get('/:id', async (req, res) => {
  try {
    const { user } = req;

    const response = await fetch(`https://rickandmortyapi.com/api/character/${req.params.id}`);
    const data = await response.json();
    const characterId = data.id;

    const favorite = await Favorite.findOne({ userId: user.id });
    if (favorite) {
      const isFavorite = favorite.characterIds.includes(characterId);
      data.isFavorite = isFavorite;
    }

    return res.status(200).json({ data });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
