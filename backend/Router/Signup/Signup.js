import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import User from '../../Models/User.js';

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const { username, password } = req.body;

    const id = uuidv4();
    const hashedPassword = await bcrypt.hash(password, 10);

    const usernameExist = await User.find({ username: username });

    if (usernameExist.length) {
      return res.status(401).json({ message: 'Username is taken.' });
    }

    const user = new User({
      id,
      username,
      password: hashedPassword,
    });

    await user.save();

    const signInCredentials = { username, id };
    const token = jwt.sign(signInCredentials, process.env.APP_SECRET_KEY);

    return res.status(201).json({ data: { user: { username }, token } });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
