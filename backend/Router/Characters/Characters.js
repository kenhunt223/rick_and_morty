import express from 'express';
import Favorite from '../../Models/Favorite.js';
import fetch from 'node-fetch';

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const { user } = req;

    const response = await fetch('https://rickandmortyapi.com/api/character');
    const { results: data } = await response.json();

    const favorites = await Favorite.find();
    if (favorites.length) {
      const usersFavorites = favorites.find(({ userId }) => userId === user.id);

      if (usersFavorites && usersFavorites.characterIds.length) {
        data.forEach((character) => {
          const isFavorite = usersFavorites.characterIds.includes(character.id);
          character.isFavorite = isFavorite;
        });
      }
    }

    return res.status(200).json({ data });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
