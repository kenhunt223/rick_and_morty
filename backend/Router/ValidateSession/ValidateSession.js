import express from 'express';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const { headers } = req || {};
    const { authorization } = headers || {};

    const token = authorization && authorization.split(' ')[1];
    if (!token) return res.status(401).json({ message: 'No token provided' });

    jwt.verify(token, process.env.APP_SECRET_KEY, (err, user) => {
      if (err) return res.status(403).json({ message: 'Invalid token' });

      return res
        .status(201)
        .json({ message: 'Token verified', data: { user: { username: user.username }, token } });
    });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
