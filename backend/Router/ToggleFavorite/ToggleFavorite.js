import express from 'express';
import Favorite from '../../Models/Favorite.js';

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const { id: characterId } = req.body;
    const { user } = req;

    const favorite = await Favorite.findOne({ userId: user.id });

    let isFavorite = false;
    if (!favorite) {
      const newFavorite = new Favorite({
        userId: user.id,
        characterIds: [characterId],
      });

      await newFavorite.save();
      isFavorite = true;
    } else {
      isFavorite = favorite.characterIds.includes(characterId);

      if (isFavorite) {
        const index = favorite.characterIds.indexOf(characterId);
        favorite.characterIds.splice(index, 1);
        isFavorite = false;
      } else {
        favorite.characterIds.push(characterId);
        isFavorite = true;
      }

      await favorite.save();
    }

    return res
      .status(201)
      .json({ message: 'Favorite has been updated.', data: { isFavorite, id: characterId } });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
