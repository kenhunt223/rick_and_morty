import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import User from '../../Models/User.js';

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const { username, password } = req.body;

    const filteredUsers = await User.find({ username: username });

    let matchedUser = undefined;
    if (filteredUsers.length > 0) {
      const promises = filteredUsers.map(async (storedUser) => {
        const isCorrectPassword = await bcrypt.compare(password, storedUser.password);

        if (isCorrectPassword) return storedUser;
      });

      matchedUser = await Promise.all(promises);
      matchedUser = matchedUser.find(Boolean);
    }

    if (!matchedUser) {
      return res.status(401).json({ message: 'Username or password are incorrect!' });
    }

    const signInCredentials = { username, id: matchedUser.id };
    const token = jwt.sign(signInCredentials, process.env.APP_SECRET_KEY);

    return res.status(201).json({ data: { user: { username }, token } });
  } catch (err) {
    return res.status(500).json({ message: JSON.stringify(err) });
  }
});

export default router;
