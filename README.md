## Hi. I'm Vardan, a software engineer @S|ngular

First of all thanks for the opportunity. Tried to finish as many points as i could in the given time. I still have 3 days to finish the project, before the deadline. Unfortunately, other topics in daily life are requiring my time.

However if you need me to **_fully complete_** the project (tests, pagination and image preloading), please let me know.

I'm very interested in the position and will do my best to finish it.

## Brief intro to the project.

3th party tools

Backend: besides the obvious ones, **node-fetch** and **mongoose**

- NodeFetch - to use **fetch** calls on backend.
- Mongoose - to connect to MongoDB

Frontend: None

---

On frontend **_authentication process_** is very basic, so I reused the same **form** to avoid code duplication.
In production - obviously the approach/code would look differently.

- User persistance. The session is validated on each reload
- You can add characters to the fav list on the detailed page '**_/character:id_**' as well as on the home page '**_/_** ', by clicking on the **fav** icon.

On backend, well... Did my best as a frontend dev.

Sure can do much better further on, if I dedicate it some time.

---

To all the question as well as missing parts in the project, test cases and UI improvements - would be happy to answer in person.

You can run the project by running

```
node app.js

or to debug it

nodemon app.js
```

for backend

and

```
yarn start
```

for frontend

Thanks.
